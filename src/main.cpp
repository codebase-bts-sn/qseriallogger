#include <iostream>

#include <QCoreApplication>
#include <QObject>
#include <QSerialPortInfo>
#include <QThread>

#include <appworker.h>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);


    AppWorker app;
    QThread thread;

    QObject::connect(&thread, &QThread::started, &app, &AppWorker::demarrer);
    QObject::connect(&thread, &QThread::finished, &app, &AppWorker::quitter);

    QObject::connect(&app, &AppWorker::termine, &thread, &QThread::quit);

    QObject::connect(&app, &AppWorker::termine, &app, &AppWorker::deleteLater);

    thread.start();

    return a.exec();
}

