#ifndef APPWORKER_H
#define APPWORKER_H

#include <QObject>

#include <serialport.h>

class AppWorker : public QObject
{
    Q_OBJECT
public:
    explicit AppWorker(QObject *parent = nullptr);

private :
    SerialPort * _ser;

public slots:
    void demarrer();
    void quitter();

private slots:
    void onReceptionTrame(QString trame);

signals:
    void termine();
};

#endif // APPWORKER_H
