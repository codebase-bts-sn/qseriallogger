#include <iostream>

#include <QCoreApplication>
#include <QSerialPortInfo>

#include "appworker.h"

using namespace std;

AppWorker::AppWorker(QObject *parent) : QObject(parent)
{
}

void AppWorker::demarrer()
{
    int numPort;

    QList<QSerialPortInfo> availablePorts = QSerialPortInfo::availablePorts();

    cout << "Ports serie sur la machine : " << endl;
    int i = 1;
    for(const QSerialPortInfo &port : availablePorts) {
        cout << "\t" << i++ << ") " << port.portName().toStdString() << endl;
    }

    bool quit = false;
    while( !quit ) {
        cout << endl << "Saisir le numero de port a utiliser : ";
        cin >> numPort;
        // SI format ou valeur incorrects ALORS
        if (cin.fail()
                || numPort < 1
                || numPort > availablePorts.length()
                ) {
            // Signaler erreur
            cout << "Saisie non valide" << endl;
            // Réinitialiser la saisie
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
        // SINON
        } else {
            // On continue le programme
            quit = true;
        }
    }

    QString portName = availablePorts[ numPort-1 ].portName();

    _ser = new SerialPort(portName);

    QObject::connect(_ser, &SerialPort::receptionTrame, this, &AppWorker::onReceptionTrame);

    _ser->send("Saisir les trames a envoyer en les terminant avec '\\r'");
    _ser->send("(Envoyer \"quit\" pour quitter)");

    cout << "En attente de reception..." << endl;

}

void AppWorker::quitter()
{
    delete _ser;
}

void AppWorker::onReceptionTrame(QString trame)
{
    cout << "Reception de : " << trame.toStdString() << endl;

    if(trame == "quit") {
        _ser->send("bye !");
        emit termine();
    }
}
