#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QObject>
#include <QSerialPort>

class SerialPort : public QObject
{
    Q_OBJECT
public:
    explicit SerialPort(QString serialPortName, QObject *parent = nullptr);
    void send(QString data);

private:
    QSerialPort * _serial;
    QByteArray _bufReception;

signals:
    QString receptionTrame(QString trame);

private slots:
    void onReadyRead();

};

#endif // SERIALPORT_H
