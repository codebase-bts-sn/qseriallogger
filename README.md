# QSerialLogger

Application Qt en ligne de commande qui affiche des trames reçues sur une liaison série.

La configuration de la liaison série est : 9600,N,8,1.

Les trames doivent se terminer par un '\r' (retour chariot).

L'application quitte lorsqu'elle reçoit "quit\r".

Cette application illustre aussi une des méthodes possibles pour exécuter du code dans un thread.

![Screenshot](img/screenshot.png  "Screenshot de l'apllication")

